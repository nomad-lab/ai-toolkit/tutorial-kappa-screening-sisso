#!/bin/bash -l

#SBATCH --nodes=10
#SBATCH --cpus-per-task=4
#SBATCH --ntasks-per-node=10
#SBATCH --time=16:00:00
#SBATCH --partition=general
#SBATCH --job-name=p_lp_r3_train
#SBATCH --output=p_lp_r3_train-%j.out
#SBATCH --error=p_lp_r3_train-%j.error


ulimit -s unlimited
export LD_LIBRARY_PATH=$I_MPI_ROOT/intel64//lib/:$I_MPI_ROOT/intel64//lib/release/:$LD_LIBRARY_PATH

export OMP_NUM_THREADS=$SLURM_CPUS_PER_TASK

# For pinning threads correctly:
export OMP_PLACES=cores

srun /u/tpurcell/git/cpp_sisso/bin/sisso++
sstat  -j   $SLURM_JOB_ID.batch   --format=JobID,MaxVMSize,MaxRSS,MaxRSSNode

